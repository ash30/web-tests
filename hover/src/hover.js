
/*
 * Quick and dirty hover
 *
 * each <a> has data-hoverID
 * each div has data-hoverTargets
 *
 * on click/hover
 *
 * search for elements that have data-target = data-hoverID
 * show()
 *
 * attach callback on all that hides() all elements with data-hoverTarget
 * 
 * Need to manuall clear all current pop ups on invocation
 * test zepto
 *
 * hide all other selectors, toggle this one
 *
 *
 */


// Declare plugin
(function($){

	var clear_hover = function(){
		var all_hover = $("[data-hoverTarget]");
		all_hover.hide();
	}
	
	$.fn.quick_hover = function(){
		// Clear all current hovers
		//clear_hover();
		
		// .data() doesn't recognise uppercase!
		var current_hoverID= this.data("hoverid");
		var matching_elements = $("[data-hovertarget*="+current_hoverID+"]")

		// Show matched objects
		console.log(matching_elements);	
		matching_elements.toggle(200)

		// Attaching hide callback
	
		$("body").on("click",function(event){
			matching_elements.hide()
		})
	}

	// Attach to current DOM
	console.log("here");
	$("a[data-hoverID]").on("click",function(event){
		$(this).quick_hover();
		event.stopPropagation();
	})
})($)




