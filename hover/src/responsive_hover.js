
/*
 * Quick and dirty hover
 *
 * each <a> has data-hoverID
 * each div has data-hoverTargets
 *
 * on click/hover
 *
 * search for elements that have data-target = data-hoverID
 * show()
 *
 * attach callback on all that hides() all elements with data-hoverTarget
 * 
 * Need to manuall clear all current pop ups on invocation
 * test zepto
 *
 * hide all other selectors, toggle this one
 *
 *
 * FIND ALL anchors with data attr
 * on touch
 * 	add tooltip class to child div
 *	show()
 *
 * attach to body a callback to hide menu, remove class?
 *
 * when a menu is showing and you click again: You should hide the current
 * when a menu is showing and you click another menu: You should hide the showing and start the next
 * when a menu is showing and you click its anchor, you should keep the menu open
 *
 * so the original hover should toggle state
 * it should hide all OTHER menus, before opening!
 *
 * Optimisation wise, we want to calculate the other hover ids once, and then bind with staticy lookup
 * 
 *
 *
 *
 */


// Declare plugin
(function($){

	
	var methods = {

	get_hoverID : function(){
		return this.data("hoverid")

	},
	
	select_hoverTarget : function(id){
		return $("[data-hoverTarget="+id+"]")
	},

	select_hoverTargetToggle : function(id){
		return $("[data-hoverTarget][data-hoverTarget!="+id+"]")
	},

	bind_hover: function(){
		var hoverID = this.hover("get_hoverID");
		var hoverTargets = this.hover("select_hoverTarget",hoverID)
		var other_hoverTargets = this.hover("select_hoverTargetToggle",hoverID);

		console.log(hoverID)
	
		this.on("touchstart",function(event){
			other_hoverTargets.hide();
			hoverTargets.toggle();			
			event.preventDefault();
			return false;
		})
		
	}
} // end methods
	
	$.fn.hover = function(method){
		return methods[method].apply(this,Array.prototype.slice.call(arguments,1));
	}

	// Attach to current DOM
	$("a[data-hoverID]").each(function(){$(this).hover("bind_hover")});
		

})(jQuery)




